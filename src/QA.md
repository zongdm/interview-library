# Вопрос-Ответ

## Вопросы на собеседованиях⬇️:

1. Какие типы данных существуют в JS?
2. Чем LEFT JOIN отличается от RIGHT JOIN?
3. Какие HTTP методы существуют?
4. Что такое идемпотентность?
5. В чём отличие блокирующих операций от неблокирующих?
6. Разница между types и interfaces?
7. Разница между COPY и ADD в Dockerfile?
8. Разница между git merge и git rebase?

## Ответы к вопросам⬆️:

1. number, bigint, string, boolean, null, undefind, object, symbol
2. Left join получает все записи из таблицы слева, а right join, соответственно, из таблицы справа
3. Get, Post, Put, Patch, Delete, Head, Options
4. Операция считается идемпотентной, если её многократное выполнение приводит к тому же результату, что и однократное выполнение. Например, умножение на 1 — идемпотентная операция
5. При блокирующих операциях обработчику приходится ждать выполнения текущей операции. При неблокирующих операциях обработчик продолжает работу, так как event loop берет задачи из очереди событий и выполняет их по мере готовности
6. types могут быть union; interfaces не может быть приметивным типом; если два или более интферфейса имеют одно имя, то они сливаются
7. COPY тоже самое, что и ADD, но без файлов .tar и удаленного запрос URL
8. merge создает новый коммит слияния, rebase перетрет старые коммиты, чтобы история выглядела так, чтобы ее коммиты выходили из заданного (как правило, последнего *HEAD*). Отребейзенные коммиты меняют свою дату и хеш
